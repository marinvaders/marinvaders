import logging as log
import time
import pkg_resources
import sqlite3
from datetime import date

import pandas as pd

import marinvaders as ml

# This script uses marinvaders to scrape the databases harmonised by the marinvaders package. Therefore, do not run this often as it will put pressure on the bandwidth of the databases while you do it.
# The final sqlite database will take up a lot of space, and the running time will be multiple days.

DB_NAME = "marinvaders_08-03-2023.sqlite3"  # Directory for the sqlite file this script will create. Set the date.
GISD_FILEPATH = "data/gisd_data_27-02-23.json"  # Read the MarINvaders documentation to see how you add GISD and IUCN redlist data to be used with the MarINvaders. Then add directory to your GISD data here.
REDLIST_FILEPATH = (
    "data/assessments_27-02-23.csv"  # Add directory to your IUCN data here.
)
# Setting up the log:
log_filename = "marinvaders_data_harvestor.log"
handlers = [log.FileHandler(log_filename)]

log.basicConfig(
    level=log.DEBUG,
    format="%(asctime)s - %(levelname)-8s " "[%(filename)s:%(lineno)d]  %(message)s",
    handlers=handlers,
)


def create_marinlife(eco_code):
    """
    Process marina life and store to DB
    """
    log.debug(f"Processing ecocode: {eco_code}")
    print(f"Processing ecocode: {eco_code}")

    try:
        ecoregion = ml.MarineLife(  # This function finds present species, and which of these are aliens for each ecoregion.
            eco_code, gisd_file=GISD_FILEPATH, redlist_file=REDLIST_FILEPATH
        )
        # Create sqlite database entity for ecoregion of a specific ecocode:
        with sqlite3.connect(DB_NAME) as conn:
            log.debug(
                f"ecoregion: {eco_code}; obis: {len(ecoregion._obis)}; aliens: {len(ecoregion.alien_species)}"
            )
            print(
                f"ecoregion: {eco_code}; obis: {len(ecoregion._obis)}; aliens: {len(ecoregion.alien_species)}"
            )
            ecoregion._obis.to_sql("obis", conn, index=False, if_exists="append")
            ecoregion.alien_species.drop(columns=["geometry"]).to_sql(
                "aliens", conn, index=False, if_exists="append"
            )
            affected_by_invasive = ecoregion.affected_by_invasive
            affected_by_invasive["ECO_CODE"] = eco_code
            affected_by_invasive.to_sql(
                "affected_by_invasive", conn, index=False, if_exists="append"
            )

    except Exception as e:
        log.error(e)
        print(f"create_marinlife: {e}")
    if len(ecoregion.all_species) == 0:
        print(f"WARNING: No species were found in ecoregion {eco_code}")
    else:
        print(
            f"{len(ecoregion.all_species)} species were found in ecoregion {eco_code}"
        )


# The main function runs the above function (create_marinlife) for every ecoregion in the world.
def main():
    start = time.time()
    ecoregions = ml.marine_ecoregions()
    geojson = ecoregions.to_json()
    ecoregions["geometry"] = ecoregions["geometry"].to_string()
    with sqlite3.connect(DB_NAME) as conn:
        ecoregions.to_sql(
            "ecoregions",
            conn,
            index=False,
        )
        c = conn.cursor()
        c.execute("CREATE TABLE ecoregions_geojson (geojson TEXT);")
        sql = f"INSERT INTO ecoregions_geojson (geojson) VALUES ('{geojson}');"
        c.execute(sql)
        conn.commit()
        c.close()

    eco_codes = set(
        ecoregions["ECO_CODE"].values
    )  # Here you could change which ecocodes you would like to scrape for if you do not want every ecocode. For example if you instead write: eco_codes = [20025]

    try:
        for ecocode in list(eco_codes):
            create_marinlife(
                eco_code=int(ecocode)
            )  # Running the create_marinlife for each ecoregion specified in eco_codes
    except Exception as e:
        log.error("Error: msg: {}".format(e))
        print("Error: msg: {}".format(e))

    log.debug("creating db indexes")
    print("creating db indexes")
    with sqlite3.connect(DB_NAME) as conn:
        cursor = conn.cursor()
        cursor.execute(
            "UPDATE obis SET species = CASE WHEN acceptedNameUsage NOTNULL THEN acceptedNameUsage ELSE species END"
        )
        cursor.execute(
            "UPDATE aliens SET species = CASE WHEN acceptedNameUsage NOTNULL THEN acceptedNameUsage ELSE species END"
        )

        df = pd.read_sql_query("Select DISTINCT aphiaID, species from obis", conn)
        log.debug(f"Unique species: {len(df)}")
        print(f"Unique species: {len(df)}")
        df.to_sql("species", conn, index=False)
        cursor.execute("CREATE INDEX idx_species_aphiaid ON species (aphiaID);")
        cursor.execute("CREATE INDEX idx_species_species ON species (species);")

        cursor.execute("CREATE INDEX idx_obis_aphiaid ON obis (aphiaID);")
        cursor.execute("CREATE INDEX idx_obis_ecocode ON obis (ECO_CODE);")
        cursor.execute("CREATE INDEX idx_obis_species ON obis (species);")
        cursor.execute("CREATE INDEX idx_aliens_aphiaid ON aliens (aphiaID);")
        cursor.execute("CREATE INDEX idx_aliens_ecocode ON aliens (ECO_CODE);")
        cursor.execute("CREATE INDEX idx_aliens_species ON aliens (species);")
        cursor.execute(
            "CREATE INDEX idx_affected_by_invasive_aphiaid ON affected_by_invasive (aphiaID);"
        )
        cursor.execute(
            "CREATE INDEX idx_affected_by_invasive_ecocode ON affected_by_invasive (ECO_CODE);"
        )
        cursor.execute(
            "CREATE INDEX idx_affected_by_invasive_species ON affected_by_invasive (species);"
        )
        cursor.execute("CREATE INDEX idx_ecoregions_ecocode ON ecoregions (ECO_CODE);")

        obis = pd.read_sql_query("select * from obis", conn)
        aliens = pd.read_sql_query("select * from aliens", conn)
        threatened = pd.read_sql_query("select * from affected_by_invasive", conn)
        regions = pd.read_sql_query("select * from ecoregions", conn)

        # create all species (OBIS and NatCon)
        natcon = aliens[aliens.dataset.str.contains("NatCon")]
        natcon = natcon[["aphiaID", "species", "ECO_CODE"]]
        natcon["dataset"] = "NatCon"
        obis["dataset"] = "OBIS"
        all_species = pd.concat([obis, natcon])
        all_species.to_sql("all_species", conn)
        cursor.execute("CREATE INDEX idx_all_species_aphiaid ON all_species (aphiaID);")
        cursor.execute(
            "CREATE INDEX idx_all_species_ecocode ON all_species (ECO_CODE);"
        )
        cursor.execute("CREATE INDEX idx_all_species_species ON all_species (species);")

        obis = obis[["aphiaID", "ECO_CODE"]]
        aliens = aliens[["aphiaID", "ECO_CODE"]]
        threatened = threatened[["aphiaID", "ECO_CODE"]]
        all_species = all_species[["aphiaID", "ECO_CODE"]]
        regions = regions[["ECO_CODE", "ECOREGION", "PROVINCE", "REALM"]]

        obis = obis.drop_duplicates(subset=["aphiaID", "ECO_CODE"], keep="last")
        all_species = all_species.drop_duplicates(
            subset=["aphiaID", "ECO_CODE"], keep="last"
        )
        aliens = aliens.drop_duplicates(subset=["aphiaID", "ECO_CODE"], keep="last")
        threatened = threatened.drop_duplicates(
            subset=["aphiaID", "ECO_CODE"], keep="last"
        )
        # Grouping and counting total species, species in OBIS, aliens, and threatened species.
        gobis = obis.groupby(["ECO_CODE"]).count()
        gall_species = all_species.groupby(["ECO_CODE"]).count()
        galiens = aliens.groupby(["ECO_CODE"]).count()
        gthreatened = threatened.groupby(["ECO_CODE"]).count()

        gobis.columns = ["obis"]
        galiens.columns = ["aliens"]
        gthreatened.columns = ["threatened"]
        gall_species.columns = ["all_species"]

        merged = (
            regions.merge(gobis, on="ECO_CODE", how="left")
            .merge(gall_species, on="ECO_CODE", how="left")
            .merge(galiens, on="ECO_CODE", how="left")
            .merge(gthreatened, on="ECO_CODE", how="left")
        )
        merged.to_sql(
            "overview",
            conn,
            index=False,
        )
        # Creating meta data and putting into a metadata table in the sqlite database
        cursor.execute(
            "CREATE TABLE metadata (timestamp TEXT, marinvaders_version TEXT, process_time_min TEXT);"
        )
        timestamp = date.today().strftime("%B %d, %Y")
        marinvaders_version = pkg_resources.get_distribution("marinvaders").version
        cursor.execute(
            f"INSERT INTO metadata (timestamp, marinvaders_version, process_time_min) VALUES ('{timestamp}', '{marinvaders_version}', '{str((time.time() - start)/60)}');"
        )
        conn.commit()
        cursor.close()

    log.debug(f"marinvaders database created in: {(time.time() - start)/60} minutes")
    print(f"marinvaders database created in: {(time.time() - start)/60} minutes")


main()
