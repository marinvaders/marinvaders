# Dataframe Header Reference

These are the headers used in the [Pandas](https://pandas.pydata.org/) DataFrames returned by the public methods:

- aphiaID (float64): ID of the species in the Aphia Database, provided by WoRMS
- species (object): Species name as registerd in WoRMS
- ECOREGION (object): Name of the ecoregion
- ECO_CODE (int64): Numerical id of the eco-region 
- establishmentMeans (object): Species status in the eco-region ("Alien" or 
    nan)
- dataset (object): Reporting dataset of Alien status
- geometry (object): Shapefile



