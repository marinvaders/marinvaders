# The Marinvader Scraper

Utility function for scraping the marinvader data.

## Usage
Name your database and fill in IUCN and GISD data. See [Optional IUCN data](https://marinvaders.gitlab.io/marinvaders/iucn_data/) for how you manually download IUCN and GISD data.

Expect the script to run for multiple days (6-12 days).

# The effect factor for marine invasions
This repository takes you through the acquisition of data and use of code to calculate an effect factor of marine invasive species within the Life Cycle Impact Assessment framework. Data from the IUCN and GISD is being used, but due to license issues this must be downloaded seperately for each user.

## Usage
* For acquiring some specific data from teh IUCN, get the IUCN token [here](https://apiv3.iucnredlist.org/api/v3/token).
* Then the Setup_guide.ipynb will take you through the following files with some tasks in between:
    * data/iucn/create_iucn_species_list.ipynb
    * data/iucn/create_assessed_marine_species.ipynb
* Eventually the Setup_guide.ipynb will take you to use the [MarINvaders](https://gitlab.com/marinvaders/marinvaders) toolbox.

The setup is the most time-consuming step. After setup you can run the User_guide.ipynb. This user guide also works as the how-to or methods chapter of the article (Gjedde et al., to be published). But be aware that you will not get the same results despite using the same method, as your updated data will be different.
