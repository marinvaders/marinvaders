# CHANGELOG

## 0.4.1

### Bugfixes
- Not all occurences and polygons were plotted correctly by self.plot() in __main__. This was an issue that arrised because of how the new shapely version converts from string to shapely geometry object has changed.

## 0.4.0

### Breaking changes

- Expanded the MRGID mapping to include MRGID_Sov1 codes on top of the already used MRGID_EZZ codes (search 'MRGID' in 'data_background' in docs folder for background info). MRGID_Sov1 should theoretically always be within the same space as MRGID_EEZ and so we now include more observations within the same ecoregion.

### Bugfixes

- We now ignore deprecation warnings from Shapely 1.8 using warnings.filterwarnings("ignore", message="") in __main__. This was an annoying print of many warnings especially when using the Species class or when plotting without actually being an issue.
- Fix GDAL/Fiona/Geopandas issue when running on Windows

### Codebase

- Switch to black for code formatting

## 0.3.3


### Documentation

- Spelling mistakes in the JOSS article
- Included a section on how to use ipykernel for JupyterLab

### Bugfixes

- removed pin for tables packages to work on newer python versions

## 0.3.2 (20210816)

These are mostly points raised by the JOSS review: https://github.com/openjournals/joss-reviews/issues/3575

### Documentation

- Replaced 'conda update' by 'conda install' in the readme
- Spelling and grammar following the JOSS feedback

### Bugfix

- fixed openpxyl requirment in setup.py
- fixed broken link to tutorial in the readme
- fixed broken links in the JOSS article references


## 0.3.1 (20210622)

### Bugfix

- Fixed API calls to WoRMS
- Consistent dtype int for aphiaID and ECO_CODE columns

## 0.3.0 (20210618)


### Additional functionality

- added gisd_scraper for reading and converting data from iucn-gisd

### Breaking changes

- IUCN data (GISD and redlist) becomes optional
- MarineLife.aliens renamed to MarineLife.alien_species (consitent with .all_species)
- MarineLife.affected_by_invasive only available when specifying redlist data
- MarineLife.all_species now returns a dataframe
- MarineLife and Species classes "obis" and "observations" became private ("_obis" and "_observations")
- marinelife.py renamed to main.py

### Intern

- Refactored GISD data reader
- Refactored redlist data reader (was taxonomy reader before)
- Removing lru_cache
- Multiple bug fixes

### Documentation

- Added section on IUCN data
- More details on licenses of the underlying data in the readme
- Documentation now only builds on the master branch or with a manual build
- DataFrame table header reference
- Added binder links to readme and notebook


## 0.2.3


### Docs

- finalized docs 
- added CI for doc generation - available at https://marinvaders.gitlab.io/marinvaders/
- moved tutorial notebook into the doc folder

### Tests

- added coveralls integration
- increased test coverage

## 0.2.2 (20210319)

Minor fixes to get it into conda-forge.

## 0.2.1 (20210319)

Aborted version due to PyPI failed upload

## 0.2.0 (20210319)

### API breaking changes

- hide "reported_as_aliens_and_natives" method
  
### Docs

- added mkdocs explaining the data background
- cleaned readme.md

## 0.1.0 (20210308)

### API breaking changes

- renamed reported_as_aliens_and_natives to reported_as_alien_and_native 
- renamed reported_as_aliens to reported_as_alien 
- renamed obis_elsewhere to all_occurrences 
- Added marinevaders.marinelife imports in top-level init 

### Extended fuctionality

- Species class accepts urn string 

### Development

- added CHANGELOG
- integration test with all eco-regions 
- reformated tests 
- refactored obis_elsewhere to avoid deprecation warning 

### Documentation

- reformated readme 
- refactored tutorial notebook 

## v0.0.12 (20210223)

### Bugfixes

- Typos
- Removing duplicates in in affected by invasive 

### Data

- Renamed natcon to molnar

### Development

- General refactoring

## v0.0.11 (20210211)

### Data

- New taxonomy file
- 
### Development

- Removing obsolete scripts
- 
### Bugfixes

- Typos

## Earlier versions

- see git history
